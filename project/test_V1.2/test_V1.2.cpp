// test_V1.0.cpp: 定义控制台应用程序的入口点。
//

/*
	存在的问题 :
	1.地雷没有显示					已解决，问题出现在计算雷区雷数的函数中
	2.没有行列的显示				已解决，修改Mine_show函数
	3.两次显示“再来一局吗？”		已解决，使用rewind(stdin)函数
	4.窗口自定义大小问题			已解决，使用system()函数
	5.添加程序的图标				已解决，引入icon资源
	6.界面显示剩余雷数及所用时间	未解决，使用函数及显示方法未知
	7.自定义雷区大小和地雷数量		未解决，使用宏定义应该可以解决
	8.随机数只能产生于0到9之间		未解决，分情况
	9.实现了按回车键继续，其它键退出的功能
*/

#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#define ismine 10
#define size 10
#define number 10
short int Minefiled[size][size];

void Mine_init(void)
{
	int hang, lie = 0;
	srand(time(0));
	for (int hang = 0; hang<size; hang++)
	{
		for (int lie = 0; lie<size; lie++)
		{
			Minefiled[hang][lie] = 0;
		}
	}
	int i = number;
	while (i>0)
	{
		hang = rand() % 10;		//只能产生0-9之间的随机数
		lie = rand() % 10;
		if (Minefiled[hang][lie] < ismine)
		{
			Minefiled[hang][lie] = ismine;
			i--;
		}
	}
	int lei_shu = 0;
	for (hang = 0; hang<size; hang++)
	{
		for (lie = 0; lie<size; lie++)
		{
			lei_shu = 0;
			if (Minefiled[hang][lie] >= ismine)
			{
				continue;
			}
			lei_shu = 0;
			if (hang - 1 >= 0)
			{
				if (lie - 1 >= 0)
				{
					if (Minefiled[hang - 1][lie - 1] >= ismine)
						lei_shu++;
				}
				if (Minefiled[hang - 1][lie] >= ismine)
					lei_shu++;
				if (lie + 1 <= size-1)
				{
					if (Minefiled[hang - 1][lie + 1] >= ismine)
						lei_shu++;
				}
			}
			if (hang + 1 <= size-1)
			{
				if (lie - 1 >= 0)
				{
					if (Minefiled[hang + 1][lie - 1] >= ismine)
						lei_shu++;
				}
				if (Minefiled[hang + 1][lie] >= ismine)
					lei_shu++;
				if (lie + 1 <= size-1)
				{
					if (Minefiled[hang + 1][lie + 1] >= ismine)
						lei_shu++;
				}
			}
			//上次这里忘记判断左右两边了
			if (lie - 1 >= 0)
			{
				if (Minefiled[hang][lie - 1] == ismine)
					lei_shu++;
			}
			if (lie + 1 <= size-1)
			{
				if (Minefiled[hang][lie + 1] == ismine)
					lei_shu++;
			}
			Minefiled[hang][lie] = lei_shu;
		}
	}
}

void dig(int hang, int lie)
{
	if (hang >= 0 && hang <= size-1 && lie >= 0 && lie <= size-1)
	{
		if (Minefiled[hang][lie] < 100)
		{
			Minefiled[hang][lie] += 100;
			if (Minefiled[hang][lie] == 100)
			{
				if (hang - 1 >= 0)
				{
					if (Minefiled[hang - 1][lie] <100)
						dig(hang - 1, lie);
				}
				if (hang + 1 <= 9)
				{
					if (Minefiled[hang + 1][lie] < 100)
						dig(hang + 1, lie);
				}
				if (lie - 1 >= 0)
				{
					if (Minefiled[hang][lie - 1] < 100)
						dig(hang, lie - 1);
				}
				if (lie + 1 <= 9)
				{
					if (Minefiled[hang][lie + 1] < 100)
						dig(hang, lie + 1);
				}
			}
		}
	}
}

void Mine_show(void)
{
	
	//显示首行列提示
	int hang = 0, lie = 0;
	printf("   0 ");
	for (int i = 1; i<size+1; i++)
		printf("  %d  ", i);
	printf("\n\n");
	for (int hang = 0; hang<size; hang++)
	{
		//显示每行列提示
		if (hang<size)
			printf("  %d) ", hang + 1);
		for (int lie = 0; lie<size; lie++)
		{

			if (Minefiled[hang][lie] >= 100)
			{
				if (Minefiled[hang][lie] - 100 == 0)
					printf("     ");
				else if (Minefiled[hang][lie] - 100 == ismine)
					printf("  X  ");
				else
					printf("  %d  ", Minefiled[hang][lie] - 100);
			}
			else
			{
				printf("  *  ");
			}
		}
		printf("\n\n");
	}
}

int pan_duan(void)
{
	int point = 1;
	for (int hang = 0; hang<size; hang++)
	{
		for (int lie = 0; lie<size; lie++)
		{
			if (Minefiled[hang][lie] != ismine)
			{
				if (Minefiled[hang][lie] == 100 + ismine)
					return 0;
				if (Minefiled[hang][lie] <100)
					point = 2;
			}
		}
	}
	return point;
}


int main(void)
{
	system("mode con cols=56 lines=23 ");
	int hang = 0, lie = 0;
	Mine_init();
	Mine_show();
	while (1)
	{
		rewind(stdin);	//这里把缓冲区重置，是为了防止输入yes这种情况下，字符es还留在缓冲区
		printf("请输入行和列(1~%d) : ",size);
		scanf_s("%d %d", &hang, &lie);
		dig(hang - 1, lie - 1);
		Mine_show();
		int i = pan_duan();
		if (i == 0)
		{
			system("mode con cols=56 lines=27 ");
			Mine_show();
			printf("\n\n		 ~~~~~~GameOver~~~~~~\n\n"); break;
		}
		if (i == 1)
		{
			system("mode con cols=56 lines=27 ");
			Mine_show();
			printf("\n\n		 ~~~~~~YouWin!!~~~~~~\n\n"); break;
		}
	}
	while (1)
	{
		//char x;
		printf("再来一局吗？  按任意键继续，Esc键退出 : ");
		rewind(stdin);
		//x = getchar();
		if ( _getch() == 27  )
			return 0;
		else 
			return main();
		//else;
	}
	//return 0;
}